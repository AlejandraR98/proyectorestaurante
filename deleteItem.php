<?php

    $mongo = new MongoDB\Driver\Manager();
    $id = $_GET['id'];

    $bw = new MongoDB\Driver\BulkWrite;
    $bw->delete(['_id' => new \MongoDB\BSON\ObjectID($id)]);

    $mongo->executeBulkWrite('Inventory.Inventory', $bw);

    header("Refresh: 0; url=inventory.php")

?>