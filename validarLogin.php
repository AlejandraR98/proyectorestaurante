<?php
session_start();
require_once("configuration.php");

$usuario = $_POST['username'];
$pass = $_POST['contrasena'];

$passEncrypt = crypt($pass, '$5$rounds=5000$asdfiuwiefnkjndt$');

$sql = "SELECT * FROM user WHERE username = '$usuario' AND password = '$passEncrypt'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {
    $tipo = $row['privileges'];
    $name = $row["name"];
    $lastname = $row["lastname"];
    $telephone = $row["telephone"];
    $username = $row["username"];
    $password = $row["password"];
  }
  $_SESSION['loggedin'] = true;
  $_SESSION['name'] = $name;
  $_SESSION['admin'] = $tipo;
  $_SESSION['lastname'] = $lastname;
  $_SESSION['telephone'] = $telephone;
  $_SESSION['username'] = $username;
  $_SESSION['password'] = $password;

  echo header("Location: home.php");
} else {
  echo header("Location: index.html");
}
?>

<!DOCTYPE html>

<html>

<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, 
    initial-scale=1">
  <link rel="stylesheet" href="
    https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
  <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <title>Restaurante "La Cabaña"</title>
</head>

<!-- Modificar estilo Bulma-->
<style>
  .navbar-item img {
    max-height: 60px;
  }

  .hero {
    background: black url(backgrounds/Home.png) center / cover;
  }

  .box {
    border: 2px solid #B00000;
    opacity: 0.80;
    border-radius: 30px;
  }

  footer {
    padding: 1rem 1rem 0.5rem !important;
    opacity: 0.70;
  }

  h1 {
    text-shadow: 1px 0 0 black, -1px 0 0 black, 0 1px 0 black, 0 -1px 0 black, 1px 1px black, -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black;
  }

  .image {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 65%;
  }

  .button {
    background-color: #B00000;
    opacity: 0.90;
    color: #FFFFFF;
  }
</style>

<body>
  <section class="hero is-fullheight">

    <div class="hero-head">
      <br>
      <div class="container">

        <div class="columns is-centered">
          <div class="column is-4 has-text-centered">

            <figure class="image is-squared">
              <img src="logos/logo.png">
            </figure>

            <!-- Script para validar Login. -->
            <form action="validarLogin.php" class="box" method="POST" id="boxLogin">

              <div class="field">
                <label for="" class="label has-text-centered" id="lblUsername">Username</label>

                <div class="control has-icons-left">
                  <input type="text" placeholder="Enter your username" class="input" id="iptUsername" required name="username">

                  <span class="icon is-small is-left">
                    <i class="fa fa-envelope"></i>
                  </span>

                </div>

              </div>

              <div class="field">
                <label for="" class="label has-text-centered" id="lblPassword">Password</label>

                <div class="control has-icons-left">
                  <input type="password" placeholder="*********" class="input" id="iptPassword" required name="contrasena">

                  <span class="icon is-small is-left">
                    <i class="fa fa-lock"></i>
                  </span>

                </div>

              </div>

              <div class="field has-text-centered is-large">
                <button class="button" id="btnLogin"> Login </button>
              </div>

            </form>

          </div>
        </div>

      </div>

    </div>

    <footer class="footer">

      <div class="content has-text-centered">
        <p>
          By Juan Carlos Sánchez, Roberto Betancourt and Alejandra Robles.
        </p>
      </div>

    </footer>

  </section>

</body>

</html>