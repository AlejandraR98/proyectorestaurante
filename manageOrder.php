<!DOCTYPE html>
<?php include 'configuration.php';
session_start();

if (isset($_SESSION['loggedin'])) {
} else {
	header('location: index.html');
	exit;
}

if (isset($_SESSION['loggedin']) && $_SESSION['admin'] == 1) {
	echo "<script> var privileges = 1 </script>";
} else {
	echo "<script> var privileges = 0 </script>";
}


require_once('connection.php');

if (isset($_POST['id_order'])) {
	$order = $_POST['id_order'];
	$_SESSION['order_details'] = $order;
	$order_details = pg_query($conn, "SELECT * FROM ORDER_AND_DISHES WHERE id_order = $order");
	$get_total = pg_query($conn, "SELECT SUM(subtotal) FROM ORDER_AND_DISHES WHERE id_order = $order");
	$total = pg_fetch_result($get_total, 0, 'sum');
} else {
	$order = $_SESSION['order_details'];
	$order_details = pg_query($conn, "SELECT * FROM ORDER_AND_DISHES WHERE id_order = $order");
	$get_total = pg_query($conn, "SELECT SUM(subtotal) FROM ORDER_AND_DISHES WHERE id_order = $order");
	$total = pg_fetch_result($get_total, 0, 'sum');
}

if (isset($_POST['delete_order'])) {
	var_dump($_POST);
	$id_order = $_POST['order'];
	$check_invoice = pg_query($conn, "SELECT EXISTS (SELECT * FROM business_logic.invoice WHERE id_order=1)");
	$invoice_associated = pg_fetch_result($check_invoice, 0, 'exists');

	if ($invoice_associated == 'f') {
		$delete_order = pg_query($conn, "DELETE FROM business_logic.restaurant_order WHERE id_order= $id_order");
	} else {
		$delete_invoice = pg_query($conn, "DELETE FROM business_logic.invoice WHERE id_order= $id_order");
		$delete_order = pg_query($conn, "DELETE FROM business_logic.restaurant_order WHERE id_order= $id_order");
	}
	echo "<script> let flag = 1; </script>";
}

if (isset($_POST['delete_all'])) {
	$id_order = $_POST['order_id'];
	$id_dish = $_POST['dish_id'];
	$delete_all = pg_query($conn, "DELETE FROM business_logic.order_list WHERE id_order=$id_order AND id_dish=$id_dish");

	echo "<script> let flag = 2; </script>";
}

if (isset($_POST['delete_one'])) {
	$id_order = $_POST['order_id'];
	$id_dish = $_POST['dish_id'];
	$quantity = $_POST['quantity'];

	if ($quantity == 1) {
		$delete_all = pg_query($conn, "DELETE FROM business_logic.order_list WHERE id_order=$id_order AND id_dish=$id_dish");
	} else {
		$quantity = $quantity - 1;
		$update_query = pg_query($conn, "UPDATE business_logic.order_list SET quantity_dish = $quantity WHERE id_order=$id_order AND id_dish=$id_dish");
	}

	echo "<script> let flag = 2; </script>";
}


?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.red {
		background-color: #B00000
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	th {
		text-align: center !important;
		background-color: #580404;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}

	.label-style {
		display: inline-block;
		vertical-align: bottom;
		padding-bottom: 7px;
		font-size: 17px;
		font-weight: bold;
		margin-right: 10px;
	}
</style>


<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<div class="hero-body">
		<div class="container has-text-centered">
			<h1 class="title has-text-centered is-fullwidth" id="ttlOrderDetails">ORDER DETAILS</h1>
		</div>
	</div>


	<div class="columns is-centered">
		<div class="column is-7">
			<div class="table-container">

				<form action="selectSection.php" method="post">
					<div class="control is-bottom">
						<input type="hidden" name="order" value="<?php echo $order ?>" id="order">
						<button class="button is-success is-size-6 has-text-white is-rounded is-pulled-right has-text-weight-bold" type="submit" name='add_dish' id="btnAddDish">+</button>
					</div>
				</form>

				<form action="manageOrder.php" method="post">
					<input type="hidden" name="order" value="<?php echo $order ?>">
					<button class="button red has-text-white is-rounded is-pulled-left" type="submit" name='delete_order' id="btnDeleteOrder">Delete Order</button>
				</form>



				<br>
				<br>
				<table width="100%" id=table class="table table-header-black">

					<tr class="has-text-white">
						<th class="has-text-white">Quantity</th>
						<th class="has-text-white">Dish</th>
						<th class="has-text-white">Unit Price</th>
						<th class="has-text-white">Subtotal</th>
						<th class="has-text-white">Manage dish</th>
					</tr>
					<?php while ($row = pg_fetch_row($order_details)) { ?>
						<tr>
							<td><?php echo $row[7] ?></td>
							<td><?php echo $row[5] ?></td>
							<td><?php echo "$" . number_format($row[6], 2, '.', ',');  ?></td>
							<td><?php echo "$" . number_format($row[8], 2, '.', ',');  ?></td>

							<td class="has-text-white">
								<form action="manageOrder.php" method="post">
									<input type="hidden" name="order_id" value="<?php echo $row[0] ?>" id="order_id">
									<input type="hidden" name="id_order" value="<?php echo $row[0] ?>" id="id_order">
									<input type="hidden" name="dish_id" value="<?php echo $row[9] ?>" id="dish_id">
									<input type="hidden" name="quantity" value="<?php echo $row[7] ?>" id="quantity">
									<button class="button is-text is-large" type="submit" name="delete_one" id="btnDeleteOne">
										<a class="icon is-large">
											<img src="icons/icon-delete.png">
										</a>
									</button>
								</form>
							</td>
						</tr>

					<?php } ?>
				</table>
				<form action="manageOrder.php" method="post">
					<input type="hidden" name="order_id" value="<?php echo $row[0] ?>" id="order_id2">
					<input type="hidden" name="id_order" value="<?php echo $row[0] ?>" id="dish_id2">
					<input type="hidden" name="dish_id" value="<?php echo $row[9] ?>" id="quantity2">
					<br>

				</form>
				<p class="subtitle is-3 has-text-right" id="pTotal">Total: $<?php echo $total ?></p>
				<br>
				<br>
			</div>
		</div>
	</div>


</body>

<script>
	if (typeof(flag) !== 'undefined') {
		if (flag == 1) {
			Swal.fire({
				title: 'Order deleted succesfully',
				icon: 'success',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Finish'
			}).then((result) => {
				if (result.value) {
					window.location.href = "showOrder.php";
				}
			})
		}

		if (flag == 2) {
			Swal.fire({
				title: 'Dish deleted successfully!',
				icon: 'success',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Finish'
			}).then((result) => {
				if (result.value) {
					window.location = window.location.href;
				}
			})

		}
	}


	if (privileges == 0) {
		$("#button_finances").addClass("is-hidden");
		$("#button_administration").addClass("is-hidden");
		$("#button_inventory").addClass("is-hidden");
	}
</script>

</html>