<!DOCTYPE html>
<?php include 'configuration.php';
session_start();

if (isset($_SESSION['loggedin'])) {
} else {
	header('location: index.html');
	exit;
}

if (isset($_SESSION['loggedin']) && $_SESSION['admin'] == 1) {
	echo "<script> var privileges = 1 </script>";
} else {
	echo "<script> var privileges = 0 </script>";
}



require_once('connection.php');

if (isset($_POST['submit'])) {
	$order = $_POST['order'];
	$checkOrderID = pg_query($conn, "SELECT EXISTS (SELECT * FROM business_logic.restaurant_order WHERE id_order = $order)");
	$orderExists = pg_fetch_result($checkOrderID, 0, 'exists');

	$id_order = $_SESSION['id_order'];
	$order_details = pg_query($conn, "SELECT * FROM ORDER_AND_DISHES WHERE id_order = $order");
	$get_total = pg_query($conn, "SELECT SUM(subtotal) FROM ORDER_AND_DISHES WHERE id_order = $order");
	$total = pg_fetch_result($get_total, 0, 'sum');
}

if (isset($_POST['create_invoice'])) {
	$order = $_POST['order'];
	$checkOrderID = pg_query($conn, "SELECT EXISTS (SELECT * FROM business_logic.restaurant_order WHERE id_order = $order)");
	$orderExists = pg_fetch_result($checkOrderID, 0, 'exists');

	$id_order = $_SESSION['id_order'];
	$order_details = pg_query($conn, "SELECT * FROM ORDER_AND_DISHES WHERE id_order = $order");
	$get_total = pg_query($conn, "SELECT SUM(subtotal) FROM ORDER_AND_DISHES WHERE id_order = $order");
	$total = pg_fetch_result($get_total, 0, 'sum');

	$payment_method = $_POST['payment_method'];
	$check_invoice = pg_query($conn, "SELECT EXISTS (SELECT * FROM business_logic.invoice WHERE id_order = $order)");
	$invoice_exists = pg_fetch_result($check_invoice, 0, 'exists');

	if ($invoice_exists == 'f') {
		$invoice_query = pg_query($conn, "INSERT INTO business_logic.invoice (payment_method, total_amount, id_order) VALUES ('$payment_method', $total, $order)");
	} else {
		$invoice_query = pg_query($conn, "UPDATE business_logic.invoice SET payment_method = '$payment_method', total_amount = $total WHERE id_order = $order");
	}

	echo "<script>let flag = 1;</script>";
}

?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.red {
		background-color: #B00000
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	th {
		text-align: center !important;
		background-color: #580404;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}

	.distribuir-campos {
		display: flex;
		justify-content: space-between;
	}

	.label-style {
		display: inline-block;
		vertical-align: bottom;
		padding-bottom: 7px;
		font-size: 17px;
		font-weight: bold;
		margin-right: 10px;
	}
</style>


<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<div class="hero-body">
		<div class="container has-text-centered">
			<h1 class="title has-text-centered is-fullwidth" id="ttlInvoiceDetails">INVOICE DETAILS</h1>
		</div>
	</div>

	<?php if ($orderExists == 't') { ?>
		<div class="columns is-centered">
			<div class="column is-7">
				<div class="table-container">

					<form action="orderDetailsInvoice.php" method="post">
						<div class="control is-bottom distribuir-campos">
							<input type="hidden" name="order" value="<?php echo $order ?>">

							<div class="metodo-de-pago" style="white-space:nowrap">
								<label class="label-style" for="payment_method">Payment method:</label>
								<div class="select">
									<select name="payment_method" id="payment_method" required>
										<option value="" selected disabled>Select one option</option>
										<option value="Cash" id="optCash">Cash</option>
										<option value="Debit card" id="optDebit">Debit card</option>
										<option value="Credit card" id="optCredit">Credit card</option>
									</select>
								</div>
							</div>

							<button class="button red has-text-white is-rounded" type="submit" name="create_invoice" id="btnCreateInvoice">$</button>
						</div>
					</form>

					<br>
					<table width="100%" id=table class="table table-header-black" hidden>

						<tr class="has-text-white">
							<th class="has-text-white">Quantity</th>
							<th class="has-text-white">Dish</th>
							<th class="has-text-white">Unit Price</th>
							<th class="has-text-white">Subtotal</th>
						</tr>
						<?php while ($row = pg_fetch_row($order_details)) { ?>
							<tr>
								<td><?php echo $row[7] ?></td>
								<td><?php echo $row[5] ?></td>
								<td><?php echo "$" . number_format($row[6], 2, '.', ',');  ?></td>
								<td><?php echo "$" . number_format($row[8], 2, '.', ',');  ?></td>
							</tr>

						<?php } ?>
					</table>
					<p class="subtitle is-3 has-text-right">Total: $<?php echo $total ?></p>
					<br>

				</div>
			</div>
		</div>

	<?php } else { ?>
		<h1 class="subtitle is-2 has-text-centered">Order number does not exist.</h1>
	<?php } ?>



</body>

<script>
	var table = document.getElementById("table");
	table.removeAttribute("hidden");

	if (typeof(flag) !== 'undefined') {
		if (flag == 1) {
			Swal.fire({
				title: 'Invoice created succesfully',
				icon: 'success',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Pay'
			}).then((result) => {
				if (result.value) {
					window.location.href = "home.php";
				}
			})
		}
	}

	if (privileges == 0) {
		$("#button_finances").addClass("is-hidden");
		$("#button_administration").addClass("is-hidden");
		$("#button_inventory").addClass("is-hidden");
	}
</script>

</html>