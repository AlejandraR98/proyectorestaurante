<!DOCTYPE html>
<?php

require_once("configuration.php");
session_start();

if (isset($_SESSION['loggedin'])) {
} else {
	header('location: index.html');
	exit;
}


require_once('connection.php');

if (isset($_POST['submit'])) {
	$nombre = $_POST['nombre'];
	$seccion = $_POST['seccion'];
	$intPrice = $_POST['intPrice'];
	$pubPrice = $_POST['pubPrice'];
	$seccion = $_POST['seccion'];
	$profit = $pubPrice - $intPrice;

	// Query para crear platillo 
	$sql_query = pg_query($conn, "INSERT INTO business_logic.dish (name_dish, internal_price, public_price, profit, id_section) VALUES	('$nombre', $intPrice, $pubPrice, $profit, $seccion)");

	if ($sql_query) {
		echo "<script> let flag = 1; </script>";
	} else {
		echo "<script> let flag = 2; </script>";
	}
}

/* Obtener secciones */
$sections = pg_query($conn, "SELECT * FROM business_logic.section");
?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}

	.button {
		background-color: #B00000
	}

	.choose {
		display: none;
	}

	.width-100 {
		width: 100%;
	}
</style>

<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>

	<section class="hero-body">
		<div class="container">
			<h1 class="title has-text-centered is-fullwidth" id="ttlCreateDish">CREATE DISH</h1>
			<div class="columns is-centered">
				<div class="column is-6">

					<form action="newDish.php" class="box" method="POST" name="dishForm" onsubmit="return validateForm()" id="boxCreateDish">
						<div class="field">
							<label class="label">Name</label>
							<div class="control">
								<input class="input" type="text" placeholder="Ex. Soup" required name="nombre" id="nombre">
							</div>
						</div>

						<div class="field">
							<label class="label">Section</label>
							<div class="control">
								<div class="select width-100">
									<select class="width-100" name="seccion" id="seccion" required>
										<option value="" disabled selected class="choose">
											Please select in item in the list
										</option>
										<?php while ($row = pg_fetch_row($sections)) { ?>
											<option value="<?php echo $row[0] ?>">
												<?php echo $row[1] ?>
											</option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="field">
							<label class="label">Internal Price</label>
							<div class="control">
								<input class="input" type="text" placeholder="Ex. 40" required name="intPrice" id="intPriceDish">
							</div>
						</div>

						<div class="field">
							<label class="label">Public Price</label>
							<div class="control">
								<input class="input" type="text" placeholder="Ex. 100" required name="pubPrice" id="pubPricDish">
							</div>
						</div>


						<br>

						<div class="field has-text-centered">
							<div class="control has-text-centered">
								<button class="button gray has-text-white is-rounded" type="submit" name="submit" id="btnCreateDish">Create</button>
							</div>
					</form>

				</div>
			</div>

		</div>
	</section>

</body>

<script>
	function validateForm() {
		let intPrice = document.dishForm.intPrice.value;
		let pubPrice = document.dishForm.pubPrice.value;

		if (intPrice < 0 || pubPrice < 0) {
			alert("Price can't be negative");
			return false;
		}
	}

	if (typeof flag !== 'undefined') {
		if (flag == 1) {
			Swal.fire({
				title: 'Dish created successfully!',
				icon: 'success',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Finish'
			}).then((result) => {
				if (result.value) {
					window.location.href = "administration.php";
				}
			})
		} else {
			Swal.fire({
				title: "Error. Try again!",
				icon: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Finish'
			}).then((result) => {
				if (result.value) {
					window.location.href = "administration.php";
				}
			})
		}
	}
</script>

</html>