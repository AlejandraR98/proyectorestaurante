<!DOCTYPE html>
<?php

require_once("configuration.php");
session_start();

if (isset($_SESSION['loggedin'])) {
	$userSesion = $_SESSION['username'];
} else {
	header('location: index.html');
	exit;
}

if (isset($_SESSION['loggedin']) && $_SESSION['admin'] == 1) {
	echo "<script> var privileges = 1 </script>";
} else {
	echo "<script> var privileges = 0 </script>";
}


require_once('connection.php');

$id_query = pg_query($conn, "SELECT getNextID()");
$id = pg_fetch_result($id_query, 0, 'getnextid');
$date_query = pg_query($conn, "SELECT CURRENT_DATE");
$date = pg_fetch_result($date_query, 0, 'current_date');

if (isset($_POST['add_dish'])) {
	$_SESSION['id_order'] = $_POST['order'];
	$_SESSION['action'] = 'edit';
}

if (isset($_POST['submit'])) {
	$number_table = $_POST['table'];
	$number_people = $_POST['persons'];

	// Query para crear orden 
	$sql_query = pg_query($conn, "CALL createOrder(date_trunc('second', now()::timestamp), '$userSesion', ($number_table, $number_people))");
	$_SESSION['action'] = 'create';
	$_SESSION['id_order'] = $id;
}

if (isset($_POST['submit_dish'])) {
	$id_dish = $_POST['submit_dish'];
	$id_order = $_SESSION['id_order'];

	// Query para crear orden 
	$sql_query = pg_query($conn, "CALL addDishToOrder($id_dish, $id_order)");
}

if (isset($_POST['submit_dessert'])) {
	$id_dish = $_POST['submit_dessert'];
	$id_order = $_SESSION['id_order'];

	// Query para crear orden 
	$sql_query = pg_query($conn, "CALL addDishToOrder($id_dish, $id_order)");
}
?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}

	.red {
		background-color: #B00000
	}
</style>

<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<!-- Cuerpo -->
	<section class="hero-body">

		<div class="columns is-centered">
			<div class="column is-6">
				<h1 class="title has-text-centered is-fullwidth" id="ttlSections">SECTIONS</h1>
				<hr>
			</div>
		</div>

		<div class="columns is-centered">
			<div class="column is-3 has-text-centered">
				<a href="selectMain.php">
					<img src="icons/icon-mainDishes.png" id="btnSelectMain">
				</a>
			</div>

			<div class="column is-3 has-text-centered">
				<a href="selectDessert.php">
					<img src="icons/icon-desserts.png" id="btnSelectDessert">
				</a>
			</div>

			<div class="column is-3 has-text-centered">
				<a href="selectDrink.php">
					<img src="icons/icon-drinks.png" id="btnSelectDrink">
				</a>
			</div>
		</div>

		<div class="column is-pulled-right">
			<br>
			<br>
			<div class="control has-text-right is-bottom">
				<a class="button red has-text-white is-rounded" href="orderDetails.php" id="btnFinish">Finish</a>
			</div>
		</div>

	</section>


</body>

<script>
	if (privileges == 0) {
		$("#button_finances").addClass("is-hidden");
		$("#button_administration").addClass("is-hidden");
		$("#button_inventory").addClass("is-hidden");
	}
</script>

</html>