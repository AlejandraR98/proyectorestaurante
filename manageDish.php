<!DOCTYPE html>
<?php

require_once("configuration.php");
session_start();

if (isset($_SESSION['loggedin'])) {
} else {
	header('location: index.html');
	exit;
}

if (isset($_SESSION['loggedin']) && $_SESSION['admin'] == 1) {
	echo "<script> var privileges = 1 </script>";
} else {
	echo "<script> var privileges = 0 </script>";
}

require_once('connection.php');
$dish = pg_query($conn, "SELECT * FROM business_logic.dish NATURAL JOIN business_logic.section ORDER BY id_dish");

if (isset($_POST['delete_dish'])) {
	$id_dish = $_POST['delete_dish'];

	$check_dish = pg_query($conn, "SELECT EXISTS (SELECT * FROM business_logic.order_list WHERE id_dish=$id_dish)");
	$dish_exists = pg_fetch_result($check_dish, 0, 'exists');

	if ($dish_exists == 't') {
		echo "<script> let flag = 1; </script>";
	} else {
		$check_dish = pg_query($conn, "DELETE FROM business_logic.dish WHERE id_dish=$id_dish");
		echo "<script> let flag = 2; </script>";
	}
}

?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	th {
		text-align: center !important;
		background-color: #580404;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}
</style>

<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<div class="hero-body">
		<div class="container has-text-centered">
			<h1 class="title has-text-centered is-fullwidth" id="ttlManageOrders">MANAGE DISHES</h1>
		</div>
	</div>

	<div class="columns is-centered">
		<div class="column is-9">
			<div class="table-container">

				<table width="100%" id=table class="table table-header-black">

					<tr class="has-text-white">
						<th class="has-text-white">Dish</th>
						<th class="has-text-white">Internal price</th>
						<th class="has-text-white">Public price</th>
						<th class="has-text-white">Section</th>
						<th class="has-text-white">Actions</th>
					</tr>
					<?php while ($row = pg_fetch_row($dish)) { ?>
						<tr>
							<td><?php echo $row[2] ?></td>
							<td><?php echo '$' . number_format($row[3], 2, '.', ','); ?></td>
							<td><?php echo '$' . number_format($row[4], 2, '.', ','); ?></td>
							<td><?php echo $row[6] ?></td>
							<td>
								<div class="control">
									<form action="editDish.php" method="post">
										<button class="button is-text is-large is-pulled-left" type="submit" name="edit_dish" value="<?php echo $row[1] ?>" id="btnEditDish_<?php echo $row[1] ?>">
											<a class="icon is-large">
												<img src="icons/icon-edit.png">
											</a>
										</button>
									</form>

									<form action="manageDish.php" method="post">
										<button class="button is-text is-large is-pulled-left" type="submit" name="delete_dish" value="<?php echo $row[1] ?>" id="btnDeletetDish_<?php echo $row[1] ?>">
											<a class="icon is-large">
												<img src="icons/icon-delete.png">
											</a>
										</button>
									</form>

								</div>
							</td>
						</tr>

					<?php } ?>
				</table>
				<br>

			</div>
		</div>
	</div>


</body>

<script>
	if (typeof flag !== 'undefined') {
		if (flag == 1) {
			Swal.fire({
				title: 'Dish is contained in order. It can not be deleted.',
				icon: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Finish'
			}).then((result) => {
				if (result.value) {
					window.location.href = "administration.php";
				}
			})
		}
		if (flag == 2) {
			Swal.fire({
				title: 'Dish was successfully deleted!',
				icon: 'success',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Finish'
			}).then((result) => {
				if (result.value) {
					window.location = window.location.href;
				}
			})
		}
	}

	if (privileges == 0) {
		$("#button_finances").addClass("is-hidden");
		$("#button_administration").addClass("is-hidden");
		$("#button_inventory").addClass("is-hidden");
	}
</script>

</html>