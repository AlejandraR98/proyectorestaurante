<!DOCTYPE html>
<?php

require_once("configuration.php");
session_start();

if (isset($_SESSION['loggedin'])) {
	if ($_SESSION['admin'] == 1);
	else {
		header("location: home.php");
	}
} else {
	header('location: index.html');
	exit;
}


require_once('connection.php');

$invoices = pg_query($conn, "SELECT * FROM business_logic.invoice ORDER BY id_invoice DESC");

?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	th {
		text-align: center !important;
		background-color: #580404;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}
</style>

<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<!-- Cuerpo -->
	<section class="hero-body">

		<div class="columns is-centered">
			<div class="column is-6">
				<h1 class="title has-text-centered is-fullwidth" id="ttlFinances">FINANCES</h1>
				<hr>
			</div>
		</div>

		<div class="columns is-centered">
			<div class="column is-7">
				<div class="table-container">

					<table width="100%" id=table class="table table-header-black">

						<tr class="has-text-white">
							<th class="has-text-white" id="idInvoice">Invoice ID</th>
							<th class="has-text-white" id="paymentM">Payment method</th>
							<th class="has-text-white" id="idOrder">Order ID</th>
							<th class="has-text-white" id="totalAmount">Total amount</th>
						</tr>
						<?php while ($row = pg_fetch_row($invoices)) { ?>
							<tr>
								<td><?php echo $row[0] ?></td>
								<td><?php echo $row[1] ?></td>
								<td><?php echo $row[3] ?></td>
								<td><?php echo $row[2] ?></td>
							</tr>

						<?php } ?>
					</table>
					<br>

				</div>
			</div>
		</div>
	</section>


</body>

</html>