<!DOCTYPE html>
<?php include 'configuration.php';
session_start();

if (isset($_SESSION['loggedin'])) {
} else {
	header('location: index.html');
	exit;
}


if (isset($_SESSION['loggedin']) && $_SESSION['admin'] == 1) {
	echo "<script> var privileges = 1 </script>";
} else {
	echo "<script> var privileges = 0 </script>";
}


require_once('connection.php');

$order = pg_query($conn, "SELECT business_logic.restaurant_order.id_order,
	business_logic.restaurant_order.date_time,
	business_logic.restaurant_order.id_user,
	(business_logic.restaurant_order.table_struct).table_number,
	(business_logic.restaurant_order.table_struct).number_people
	FROM business_logic.restaurant_order ORDER BY business_logic.restaurant_order.id_order DESC");

?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	th {
		text-align: center !important;
		background-color: #580404;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}
</style>


<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<div class="hero-body">
		<div class="container has-text-centered">
			<h1 class="title has-text-centered is-fullwidth" id="ttlOrders">ORDERS</h1>
		</div>
	</div>

	<div class="columns is-centered">
		<div class="column is-7">
			<div class="table-container">

				<table width="100%" id=table class="table table-header-black" hidden>

					<tr class="has-text-white">
						<th class="has-text-white">Order No.</th>
						<th class="has-text-white">Date</th>
						<th class="has-text-white">Table No.</th>
						<th class="has-text-white">Persons No.</th>
						<th class="has-text-white">Manage order</th>
					</tr>
					<?php while ($row = pg_fetch_row($order)) { ?>
						<tr>
							<td><?php echo $row[0] ?></td>
							<td><?php echo $row[1] ?></td>
							<td><?php echo $row[3] ?></td>
							<td><?php echo $row[4] ?></td>

							<td class="has-text-white">
								<form action="manageOrder.php" method="post">
									<button class="button is-text is-large" type="submit" name="id_order" id="id_order" value="<?php echo $row[0] ?>">
										<a class="icon is-large">
											<img src="icons/icon-edit.png">
										</a>
									</button>
								</form>
							</td>
						</tr>

					<?php } ?>
				</table>
				<br>

			</div>
		</div>
	</div>

</body>

<script>
	var table = document.getElementById("table");
	table.removeAttribute("hidden");

	if (privileges == 0) {
		$("#button_finances").addClass("is-hidden");
		$("#button_administration").addClass("is-hidden");
		$("#button_inventory").addClass("is-hidden");
	}
</script>

</html>