<!DOCTYPE html>
<?php

require_once("configuration.php");
session_start();

if (isset($_SESSION['loggedin'])) {
	$userSesion = $_SESSION['username'];
	$sql = "SELECT * from user WHERE username='$userSesion'";
	$conn->query($sql);
} else {
	header('location: index.html');
	exit;
}

if (isset($_SESSION['loggedin']) && $_SESSION['admin'] == 1) {
	echo "<script> var privileges = 1 </script>";
} else {
	echo "<script> var privileges = 0 </script>";
}

require_once('connection.php');

?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.red {
		background-color: #B00000
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}

	.button {
		background-color: #B00000
	}
</style>

<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>

	<section class="hero-body">
		<div class="container">
			<h1 class="title has-text-centered is-fullwidth"  id="ttlCreateInvoice">CREATE INVOICE</h1>
			<div class="columns is-centered">
				<div class="column is-6">

					<form action="orderDetailsInvoice.php" class="box" method="POST" name="invoiceForm" id="boxInvoiceForm">
						<div class="field">
							<label class="label">Order No.</label>
							<div class="control">
								<input class="input" type="text" placeholder="Ex. 1" required name="order" id="iptOrderNum">
							</div>
						</div>

						<div class="field">
							<div class="control has-text-centered	">
								<button type="submit" name="submit" class="button red has-text-white is-rounded" id="btnSearch">Search</button>
							</div>
					</form>

				</div>
			</div>

		</div>
	</section>

</body>
<script>
	if (privileges == 0) {
		$("#button_finances").addClass("is-hidden");
		$("#button_administration").addClass("is-hidden");
		$("#button_inventory").addClass("is-hidden");
	}
</script>

</html>