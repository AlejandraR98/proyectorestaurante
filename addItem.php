<?php

    $mongo = new MongoDB\Driver\Manager();

    $itemName = $_POST["name"];
    $itemQTY = $_POST["quantity"];

    $bw = new MongoDB\Driver\BulkWrite;
    $doc = ["_id" => new MongoDB\BSON\ObjectID, "item" => $itemName, "qty" => $itemQTY ];
    $bw->insert($doc);
    $mongo->executeBulkWrite('Inventory.Inventory', $bw);

    header("Refresh: 0; url=inventory.php")
?>