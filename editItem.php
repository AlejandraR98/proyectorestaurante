<?php

    $bw = new MongoDB\Driver\BulkWrite;


    $id = $_POST["idEdit"];
    $itemName = $_POST["nameEdit"];
    $itemQTY = $_POST["quantityEdit"];

    $bw->update(
        ['_id' => new \MongoDB\BSON\ObjectID($id)],
        ['$set' => ['item' => $itemName, 'qty' => $itemQTY]],
    );

    $mongo = new MongoDB\Driver\Manager();
    $mongo->executeBulkWrite('Inventory.Inventory', $bw);

    header("Refresh: 0; url=inventory.php")
?>