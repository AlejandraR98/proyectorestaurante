<!DOCTYPE html>


<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<?php

require_once("configuration.php");
session_start();

if (isset($_SESSION['loggedin'])) {
	echo "<script> var bandera = 0; </script>";
	if ($_SESSION['admin'] == 1) {
		echo "<script> var privileges = 1 </script>";
	} else {
		echo "<script> var privileges = 0 </script>";
	}

	$userSesion = $_SESSION['username'];
	$sql = "SELECT * from user WHERE username='$userSesion'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$username = $row['username'];
			$name = $row['name'];
			$lastname = $row['lastname'];
			$telephone = $row['telephone'];
		}
	}
} else {
	header('location: index.html');
	exit;
}

if (isset($_POST['confirmEdit'])) {
	$nombre = $_POST["iptName"];
	$apellido = $_POST["iptLast"];
	$telefono = $_POST["iptTel"];

	$sql = "UPDATE user SET name='$nombre', lastname='$apellido', telephone='$telefono' WHERE username='$username'";
	$result = $conn->query($sql);

	$sql = "SELECT * from user WHERE username='$userSesion'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$username = $row['username'];
			$name = $row['name'];
			$lastname = $row['lastname'];
			$telephone = $row['telephone'];
		}
	}

	echo "<script> var bandera = 2; </script>";
	unset($_POST['confirmEdit']);
}

if (isset($_POST['confirmPass'])) {
	$contrasena = $_POST["iptPass"];
	$contrasenaEncriptada = crypt($contrasena, '$5$rounds=5000$asdfiuwiefnkjndt$');

	$sql = "UPDATE user SET password='$contrasenaEncriptada' WHERE username='$username'";
	$result = $conn->query($sql);

	echo "<script> var bandera = 1; </script>";
	unset($_POST['confirmPass']);
}

?>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}

	.button {
		background-color: #B00000
	}
</style>

<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<!-- Cuerpo -->
	<section class="hero-body">
		<div class="container">

			<div class="columns is-centered">
				<div class="column is-4">
					<h1 class="title has-text-centered is-fullwidth" id="ttlProfile">PROFILE</h1>

					<div class="box is-centered">

						<form action="profile.php" method="POST">
							<div class="field is-horizontal is-centered">
								<div class="field-label is-normal">
									<label class="label has-text-black is-italic ">Name:</label>
								</div>
								<div class="field-body">
									<div class="field ">
										<p class="control">
											<input class="input is-static edit" type="text" value="<?php echo $name ?>" id="iptName" name="iptName" disabled>
										</p>
									</div>
								</div>
							</div>


							<div class="field is-horizontal">
								<div class="field-label is-normal">
									<label class="label has-text-centered has-text-black is-italic">Lastname:</label>
								</div>
								<div class="field-body">
									<div class="field">
										<p class="control">
											<input class="input is-static edit" type="text" value="<?php echo $lastname ?>" id="iptLast" name="iptLast" disabled>
										</p>
									</div>
								</div>
							</div>


							<div class="field is-horizontal">
								<div class="field-label is-normal">
									<label class="label has-text-centered has-text-black is-italic">Telephone:</label>
								</div>
								<div class="field-body">
									<div class="field">
										<p class="control">
											<input class="input is-static edit" type="text" value="<?php echo $telephone ?>" id="iptTel" name="iptTel" disabled>
										</p>
									</div>
								</div>
							</div>


							<div class="field is-horizontal">
								<div class="field-label is-normal">
									<label class="label has-text-centered has-text-black is-italic">Username:</label>
								</div>
								<div class="field-body">
									<div class="field">
										<p class="control">
											<input class="input is-static" type="text" value="<?php echo $username ?>" id="iptUser" name="iptUser" disabled>
										</p>
									</div>
								</div>
							</div>


							<div class="field is-horizontal">
								<div class="field-label is-normal">
									<label class="label has-text-centered has-text-black is-italic">Password:</label>
								</div>
								<div class="field-body">
									<div class="field">
										<p class="control">
											<input class="input is-static editPass" type="password" placeholder="********" id="iptPass" name="iptPass" disabled required>
										</p>
									</div>
								</div>
							</div>
					</div>

					<input class="button is-rounded has-text-white is-pulled-left is-success is-hidden" type="submit" value="Confirm" id="confirmEdit" name="confirmEdit">
					<input class="button is-rounded has-text-white is-pulled-left is-success is-hidden" type="submit" value="Confirm" id="confirmPass" name="confirmPass">

					</form>

					<input class="button is-rounded has-text-white is-pulled-left" type="button" value="Edit Password" id="editPass" onclick="editPass()">
					<input class="button is-rounded has-text-white is-pulled-right" type="button" value="Edit" id="edit" onclick="edit()">
				</div>
			</div>
		</div>
	</section>

	<script>
		if (typeof bandera !== 'undefined') {
			if (bandera == 1) {
				Swal.fire({title: 'Password Changed!', type:'success', timer: 2000,
					showConfirmButton: false,});
				bandera = 0;
			}

			if (bandera == 2) {
				Swal.fire({
					title: "Info Changed!",
					timer: 2000,
					showConfirmButton: false,
				});
				bandera = 0;
			}
		}

		function edit() {
			$('#edit').attr("hidden", true);
			$('.edit').attr("disabled", false);
			$('.edit').removeClass("is-static");
			$('#confirmEdit').removeClass("is-hidden");
			$('#edit').addClass("is-hidden");
			$('#editPass').addClass("is-hidden");


		}

		function editPass() {
			$('#confirmPass').removeClass("is-hidden");
			$('#edit').addClass("is-hidden");
			$('#editPass').addClass("is-hidden");
			$('.editPass').attr("disabled", false);
			$('.editPass').removeClass("is-static");
		}

		if (privileges == 0) {
			$("#button_finances").addClass("is-hidden");
			$("#button_administration").addClass("is-hidden");
			$("#button_inventory").addClass("is-hidden");
		}
	</script>

</body>

</html>