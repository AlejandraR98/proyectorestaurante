<!DOCTYPE html>
<?php

require_once("configuration.php");
session_start();

if (isset($_SESSION['loggedin'])) {
} else {
	header('location: index.html');
	exit;
}


if (isset($_POST['submit'])) {
	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];
	$telefono = $_POST['telefono'];
	$usuario = $_POST['usuario'];
	$contrasena = $_POST['contrasena'];
	$contrasenaEncript = crypt($contrasena, '$5$rounds=5000$asdfiuwiefnkjndt$');


	if (isset($_POST['questionAdmin']))
		$tipo = $_POST['questionAdmin'];
	else
		$tipo = 0;

	$sql = "SELECT username FROM user WHERE username = '$usuario'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		echo "<script> var bandera = 3 </script>";
	} else {
		$sql = "INSERT INTO user (username, name, lastname, telephone, password, privileges) VALUES ('$usuario', '$nombre', '$apellido', '$telefono', '$contrasenaEncript', '$tipo')";
		if ($conn->query($sql)) {
			echo "<script> var bandera =1 </script>";
		} else {
			echo "<script> var bandera =2 </script>";
		}
	}
}
?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404;
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}

	.button {
		background-color: #B00000
	}
</style>

<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>

	<section class="hero-body">
		<div class="container">
			<div class="columns is-centered">
				<div class="column is-6">
					<h1 class="title has-text-centered is-fullwidth" id="ttlCreateUser">CREATE USER</h1>

					<form action="newUser.php" class="box" method="POST">
						<div class="field">
							<label class="label">Name</label>
							<div class="control">
								<input class="input" type="text" placeholder="Ex. Daniel" required name="nombre" id="iptNameUser">
							</div>
						</div>

						<div class="field">
							<label class="label">Lastname</label>
							<div class="control">
								<input class="input" type="text" placeholder="Ex. Perez" required name="apellido" id="iptLastnameUser">
							</div>
						</div>

						<div class="field">
							<label class="label">Telephone</label>
							<div class="control has-icons-left has-icons-right">
								<input class="input" type="text" placeholder="Ex. 2221163430" required name="telefono" id="iptTelephoneUser">
								<span class="icon is-small is-left">
									<i class="fas fa-phone"></i>
								</span>
							</div>
						</div>

						<div class="field">
							<label class="label">Username</label>
							<div class="control has-icons-left has-icons-right">
								<input class="input" type="text" placeholder="Ej. e0002" required name="usuario" id="iptUsernameUser">
								<span class="icon is-small is-left">
									<i class="fas fa-user"></i>
								</span>
							</div>
						</div>

						<div class="field">
							<label for="" class="label">Password</label>
							<div class="control has-icons-left">
								<input type="password" placeholder="*********" class="input" required name="contrasena" id="iptPasswordUser">
								<span class="icon is-small is-left">
									<i class="fa fa-lock"></i>
								</span>
							</div>
						</div>
						<br>

						<div class="field">
							<div class="control">
								<label class="checkbox">
									<input type="radio" id="si" name="questionAdmin" value="1">
									<label for="si">Administrator permissions?</label><br>
								</label>
							</div>
						</div>

						<br>

						<div class="field has-text-centered">
							<div class="control has-text-centered	">
								<button class="button gray has-text-white is-rounded" type="submit" id="submit" name="submit">Create</button>
							</div>
					</form>

				</div>
			</div>

		</div>
	</section>



</body>

<script>
	if (typeof bandera !== 'undefined') {
		if (bandera == 1) {
			Swal.fire({
				title: 'User created succesfully',
				icon: 'success',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Finish'
			}).then((result) => {
				if (result.value) {
					window.location.href = "administration.php";
				}
			})
			bandera = 0;
		}
		if (bandera == 2) {
			Swal.fire({
				title: 'Error. Try again!',
				icon: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'D=',

			}).then((result) => {
				if (result.value) {
					window.location.href = "newUser.php";
				}
			})
			bandera = 0;
		}

		if (bandera == 3) {
			Swal.fire({
				title: 'Error. Username is already registered!',
				icon: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Create other user',

			}).then((result) => {
				if (result.value) {
					window.location.href = "newUser.php";
				}
			})
			bandera = 0;
		}
	}
</script>

</html>