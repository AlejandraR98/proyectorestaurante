<?php
	ini_set('session.cache_limiter', 'public');
	session_cache_limiter(false);

	$servername = "localhost";
	$username = "admin";
	$password = "admin";
	$dbname = "mysql_lacabana";

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// Check connection
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
?>