CREATE user admin WITH ENCRYPTED PASSWORD 'admin';

/* Creación de la base de datos */
CREATE DATABASE postgres_lacabana;
GRANT all privileges ON DATABASE postgres_lacabana TO admin;

\c postgres_lacabana;

/* Creación de un tipo de dato compuesto */
CREATE TYPE TABLE_INFORMATION AS(
    table_number INT,
    number_people INT
);

/* Creación de un schema */
CREATE SCHEMA business_logic;

/* Definición de las tablas */
CREATE TABLE business_logic.section(
    id_section SERIAL PRIMARY KEY,
    name_section VARCHAR(45) NOT NULL
);

CREATE TABLE business_logic.dish(
    id_dish SERIAL PRIMARY KEY,
    name_dish VARCHAR(45) NOT NULL,
    internal_price DECIMAL NOT NULL CHECK (internal_price > 0),
    public_price DECIMAL NOT NULL CHECK (public_price > 0),
    profit DECIMAL NOT NULL,
    id_section INT NOT NULL,

    FOREIGN KEY (id_section) REFERENCES business_logic.section (id_section)
    ON DELETE SET NULL
    ON UPDATE CASCADE
);

CREATE TABLE business_logic.restaurant_order(
    id_order SERIAL PRIMARY KEY,
    date_time TIMESTAMP,
    id_user VARCHAR(45) NOT NULL,
    table_struct TABLE_INFORMATION
);

CREATE TABLE business_logic.invoice(
    id_invoice SERIAL PRIMARY KEY,
    payment_method VARCHAR(45) NOT NULL,
    total_amount DECIMAL NOT NULL CHECK (total_amount > 0),
    id_order INT NOT NULL,

    FOREIGN KEY (id_order) REFERENCES business_logic.restaurant_order (id_order)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE business_logic.order_list(
    id_order INT NOT NULL,
    id_dish INT NOT NULL,
    quantity_dish INT NOT NULL,

    PRIMARY KEY (id_order, id_dish),

    FOREIGN KEY (id_order) REFERENCES business_logic.restaurant_order (id_order)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    FOREIGN KEY (id_dish) REFERENCES business_logic.dish (id_dish)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
);

/* Función que regresa la próxima primary key de la tabla de órdenes */
CREATE FUNCTION getNextID() RETURNS INT AS $$
DECLARE
    nextID INT;
BEGIN
    SELECT last_value FROM business_logic.restaurant_order_id_order_seq
    INTO nextID;
    IF nextID IS NULL THEN
        nextID = 1;
    ELSE
        nextID = nextID + 1;
    END IF;
    RETURN nextID;
END; $$
LANGUAGE PLPGSQL;


/* Stored procedure que crea una orden */
CREATE PROCEDURE createOrder(dt TIMESTAMP, idu VARCHAR, ts TABLE_INFORMATION)
LANGUAGE plpgsql    
AS $$
BEGIN
    INSERT INTO business_logic.restaurant_order(date_time, id_user, table_struct)
    VALUES (dt, idu, ts);
END;
$$;

/* Insertar datos de sección y platillos */
INSERT INTO business_logic.section (name_section) VALUES ('Main dishes'), ('Desserts'), ('Drinks');

INSERT INTO business_logic.dish (name_dish, internal_price, public_price, profit, id_section) VALUES
('Meat', 119.00, 259.00, 140.00, 1),
('Soup', 30.00, 75.00, 45.00, 1),
('Salad', 25.00, 60.00, 35.00, 1),
('Pasta', 50.00, 95.00, 45.00, 1),
('Chocolate cake', 22.00, 48.00, 26.00, 2),
('Lemon pie', 15.00, 44.00, 29.00, 2),
('Ice cream', 20.00, 46.00, 26.00, 2),
('Soda', 15.00, 28.00, 13.00, 3),
('Lemonade', 11.00, 25.00, 14.00, 3),
('Coffee', 16.00, 30.00, 14.00, 3);


/* Se llama al stored procedure para insertar nueva orden */
CALL createOrder(date_trunc('second', now()::timestamp), 'e0001', (3, 2));


/* Transacción (dentro de un stored procedure) que agrega un platillo a una orden */
CREATE PROCEDURE addDishToOrder(idd INT, ido INT)
LANGUAGE plpgsql    
AS $$
DECLARE
    current_quantity INT;
BEGIN
    IF (SELECT EXISTS (SELECT * FROM business_logic.order_list WHERE id_order = ido AND id_dish = idd) = FALSE ) THEN
        INSERT INTO business_logic.order_list (id_dish, id_order, quantity_dish) VALUES (idd, ido, 1);
        COMMIT;

    ELSE
        SELECT quantity_dish INTO current_quantity FROM business_logic.order_list WHERE id_order = ido AND id_dish = idd;
        UPDATE business_logic.order_list SET quantity_dish = (current_quantity+1) WHERE id_order = ido AND id_dish = idd;
        COMMIT;
    END IF;
    
END;
$$;

/* Agregar platillo con id 1 a la orden con id 1 */
CALL addDishToOrder(1, 1);
CALL addDishToOrder(2, 1);

/* Crear view para desplegar los detalles de una orden */
CREATE VIEW ORDER_AND_DISHES AS
SELECT business_logic.order_list.id_order,
business_logic.restaurant_order.date_time,
(business_logic.restaurant_order.table_struct).table_number,
(business_logic.restaurant_order.table_struct).number_people,
business_logic.restaurant_order.id_user,
business_logic.dish.name_dish,
business_logic.dish.public_price,
business_logic.order_list.quantity_dish,
quantity_dish * public_price AS subtotal,
business_logic.dish.id_dish
FROM business_logic.order_list
INNER JOIN business_logic.restaurant_order
ON business_logic.order_list.id_order = business_logic.restaurant_order.id_order
INNER JOIN business_logic.dish
ON business_logic.order_list.id_dish = business_logic.dish.id_dish;


/* Conceder privilegios a schema, tablas y sequences a admin */
GRANT USAGE ON SCHEMA business_logic TO admin;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA business_logic TO admin;
GRANT USAGE, SELECT ON SEQUENCE business_logic.restaurant_order_id_order_seq TO admin;
GRANT USAGE, SELECT ON SEQUENCE business_logic.dish_id_dish_seq TO admin;
GRANT USAGE, SELECT ON SEQUENCE business_logic.invoice_id_invoice_seq TO admin;
GRANT ALL PRIVILEGES ON ORDER_AND_DISHES TO admin;