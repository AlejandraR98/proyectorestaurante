CREATE DATABASE mysql_lacabana;

USE mysql_lacabana;

CREATE TABLE IF NOT EXISTS user (
  username VARCHAR(10) NOT NULL,
  name VARCHAR(45) NOT NULL,
  lastname VARCHAR(45) NOT NULL,
  telephone VARCHAR(45) NOT NULL,
  password VARCHAR(200) NOT NULL,
  privileges INT NOT NULL,
  PRIMARY KEY (username));

INSERT INTO user (username, name, lastname, telephone, password, privileges) VALUES ('e0001', 'Alejandra', 'Robles', '2464635353', '$5$rounds=5000$asdfiuwiefnkjndt$Gih.yGX8xVR.BLMBg9dvdjKqwsfBaewxiYfMDqDHNX4', '1');
INSERT INTO user (username, name, lastname, telephone, password, privileges) VALUES ('e0002', 'Roberto', 'Betancourt', '2223445561', '$5$rounds=5000$asdfiuwiefnkjndt$YQxYv3DTs1MIpL8RbxOFsRJDmJiFVMu/BY.qMIXgFV0', '0');
INSERT INTO user (username, name, lastname, telephone, password, privileges) VALUES ('e0003', 'Juan Carlos', 'Sanchez', '2223331256', '$5$rounds=5000$asdfiuwiefnkjndt$Gih.yGX8xVR.BLMBg9dvdjKqwsfBaewxiYfMDqDHNX4', '0');
