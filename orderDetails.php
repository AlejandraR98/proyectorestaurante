<!DOCTYPE html>
<?php include 'configuration.php';
session_start();

if (isset($_SESSION['loggedin'])) {
} else {
	header('location: index.html');
	exit;
}

if (isset($_SESSION['loggedin']) && $_SESSION['admin'] == 1) {
	echo "<script> var privileges = 1 </script>";
} else {
	echo "<script> var privileges = 0 </script>";
}


require_once('connection.php');
$id_order = $_SESSION['id_order'];
$order_details = pg_query($conn, "SELECT * FROM ORDER_AND_DISHES WHERE id_order = $id_order");

if ($_SESSION['action'] == 'edit') {
	echo "<script> let flag = 1; </script>";
}

if (isset($_POST['cancel_order'])) {
	$undo_dishes = pg_query($conn, "DELETE FROM business_logic.order_list WHERE id_order = $id_order");
	$undo_order = pg_query($conn, "DELETE FROM business_logic.restaurant_order WHERE id_order = $id_order");

	echo "<script> let flag = 2; </script>";
}

?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	th {
		text-align: center !important;
		background-color: #580404;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}

	.red {
		background-color: #B00000
	}
</style>


<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head" id="navBar">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png" id="navCabaña">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white" id="button_orders">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless" id="btnAccount">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item" id="btnProfile">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item" id="btnSignOut">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<div class="hero-body">
		<div class="container has-text-centered">
			<h1 class="title has-text-centered is-fullwidth" id="ttlOrderDetails">ORDER DETAILS</h1>
		</div>
	</div>

	<div class="columns is-centered">
		<div class="column is-7">
			<div class="table-container">
				<table width="100%" id=table class="table table-header-black" hidden>

					<tr class="has-text-white">
						<th class="has-text-white">Quantity</th>
						<th class="has-text-white">Dish</th>
						<th class="has-text-white">Unit price</th>
						<th class="has-text-white">Subtotal</th>
					</tr>

					<?php while ($row = pg_fetch_row($order_details)) { ?>
						<tr>
							<td><?php echo $row[7] ?></td>
							<td><?php echo $row[5] ?></td>
							<td><?php echo "$" . number_format($row[6], 2, '.', ',');  ?></td>
							<td><?php echo "$" . number_format($row[8], 2, '.', ',');  ?></td>
						</tr>

					<?php } ?>
				</table>
				<br>

				<div class="control has-text-right is-bottom">
					<form action="orderDetails.php" method="post">
						<button id="cancel" class="button red has-text-white is-rounded is-pulled-left" name="cancel_order">Cancel</button>
						<a id="message" class="button is-success has-text-white is-rounded is-pulled-right" onclick="modal()">Create</a>
					</form>
				</div>

			</div>
		</div>
	</div>
	</div>

</body>

<script>
	var table = document.getElementById("table");
	table.removeAttribute("hidden");

	function modal() {
		Swal.fire({
			title: 'Order created succesfully',
			icon: 'success',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Finish'
		}).then((result) => {
			if (result.value) {
				window.location.href = "home.php";
			}
		})
	}

	function modalEdit() {
		Swal.fire({
			title: 'Order updated succesfully',
			icon: 'success',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Finish'
		}).then((result) => {
			if (result.value) {
				window.location.href = "home.php";
			}
		})
	}


	if (typeof(flag) !== 'undefined') {
		if (flag == 1) {
			document.getElementById('cancel').style.visibility = 'hidden';
			document.getElementById('message').innerHTML = 'Update';
			document.getElementById('message').onclick = modalEdit;
		}

		if (flag == 2) {
			Swal.fire({
			title: 'Order cancelled successfully!',
			icon: 'warning',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Finish'
		}).then((result) => {
			if (result.value) {
				window.location.href = "home.php";
			}
		})
		}
	}

	if (privileges == 0) {
		$("#button_finances").addClass("is-hidden");
		$("#button_administration").addClass("is-hidden");
		$("#button_inventory").addClass("is-hidden");
	}
</script>

</html>