<!DOCTYPE html>
<?php

require_once("configuration.php");
session_start();

if (isset($_SESSION['loggedin'])) {
	if ($_SESSION['admin'] == 1);
	else {
		header("location: home.php");
	}
} else {
	header('location: index.html');
	exit;
}

?>

<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!--link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/bootstrap.min.js"></script>-->
	<title> Restaurante "La Cabaña" </title>
</head>

<!-- Modificar estilo Bulma-->
<style>
	.navbar-item1 img {
		max-height: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item img {
		padding-inline-end: 10px;
	}

	.navbar-menu a {
		padding-inline-start: 60px;
		padding-inline-end: 60px;
	}

	.navbar-item {
		font-size: 18px;
	}

	.gray {
		background-color: #580404
	}

	.table-header-gray th {
		text-align: center !important;
		color: white;
		background-color: #580404;
	}

	td {
		text-align: center !important;
	}

	th {
		text-align: center !important;
		background-color: #580404;
	}

	.box {
		border: 2px solid #B00000;
		opacity: 0.80;
		border-radius: 30px;
	}

	.navbar-menu :hover {
		background-color: #470505 !important;
	}

	.navbar-end :hover {
		background-color: #470505 !important;
	}

	.navbar-end div div a :hover {
		background-color: red !important;
	}

	.column {
		padding-left: 3rem !important;
		padding-right: 3rem !important;
	}

	hr {
		background-color: gray !important;
	}
</style>

<body>

	<!-- Barra de Navegacion.-->
	<section class="hero-head">

		<nav class="navbar gray">

			<div class="container">

				<div class="navbar-brand">

					<div class="navbar-item1">
						<a href="home.php">
							<img src="logos/logoBlanco.png">
						</a>
					</div>

				</div>

				<div class="navbar-menu is-centered">

					<a href="home.php" class="navbar-item is-active has-text-white">
						Orders
					</a>

					<a href="finances.php" class="navbar-item has-text-white" id="button_finances">
						Finances
					</a>

					<a href="administration.php" class="navbar-item has-text-white" id="button_administration">
						Administration
					</a>

					<a href="inventory.php" class="navbar-item has-text-white" id="button_inventory">
						Inventory
					</a>

				</div>

				<div class="navbar-end">

					<div class="navbar-item has-dropdown is-hoverable has-text-white">

						<a class="navbar-link has-text-white is-arrowless">
							<img src="icons/icon-profile.png">
							<?php echo $_SESSION['username'] ?>
						</a>

						<div class="navbar-dropdown">

							<a href="profile.php" class="navbar-item">
								Profile
							</a>

							<hr class="navbar-divider">
							<a href="logout.php" class="navbar-item">
								Sign Out
							</a>

						</div>

					</div>

				</div>

			</div>

		</nav>

	</section>


	<!-- Cuerpo -->
	<section class="hero-body">
		<div class="columns is-centered">
			<div class="column is-6">
				<h1 class="title has-text-centered is-fullwidth" id="ttlOrders">INVENTORY</h1>
				<hr>
			</div>
		</div>
		<div class="container">
			<div class="columns is-centered">
				<div class="column is-6">
					<h3 class="title has-text-centered is-fullwidth">ADD NEW ITEM</h3>

					<form class="box" action="addItem.php" method="post">
						<div class="field">
							<label class="label" for="nameID">Item</label>
							<div class="control">
								<input type="text" name="name" id="nameID" class="input" placeholder="Enter the item name" required>
							</div>
						</div>

						<div class="field">
							<label class="label" for="quantityID">Quantity</label>
							<div class="control">
								<input type="number" min="0" name="quantity" id="quantityID" class="input" placeholder="Enter the number of items" required>
							</div>
						</div>

						<div class="field has-text-centered">
							<div class="control has-text-centered">
								<button type="submit" class="button gray has-text-white is-rounded">Save</button>
							</div>
						</div>

					</form>
					
					<br>
					<h3 class="title has-text-centered is-fullwidth">LIST OF ITEMS</h3>

					<table width="100%" class="table table-header-black">

						<tr class="has-text-white">
							<th class="has-text-white">Item</th>
							<th class="has-text-white">Quantity</th>
							<th class="has-text-white">Manage</th>
						</tr>

						

						<?php
						$mongo = new MongoDB\Driver\Manager();

						$qr = new MongoDB\Driver\Query([]);
						$rows = $mongo->executeQuery('Inventory.Inventory', $qr);

						foreach ($rows as $row) { ?>
							<tr>
								<td>
									<?php echo "$row->item"; ?>
								</td>

								<td>
									<?php echo "$row->qty"; ?>
								</td>

								<td>
									<a href="modifyItem.php?id=<?php echo $row->_id ?>" class="button is-rounded">Modificar</a>
									<a href="deleteItem.php?id=<?php echo $row->_id ?>" class="button is-rounded">Eliminar</a>
								</td>
							</tr>
						<?php } ?>

						

					</table>

				</div>
			</div>
		</div>
	</section>


</body>

</html>