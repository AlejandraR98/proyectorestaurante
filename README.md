Run PostgreSQL in website:
    1. Install PostgreSQL with the following commands:
        sudo apt-get install wget ca-certificates
        wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
        sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
        sudo apt-get update
        sudo apt-get install postgresql postgresql-contrib
    2. Run the commands on the postgresdb.sql file, which is located in sql folder
    3. Open php.ini, which is located in /opt/lampp/etc
    4. Find ;extension=php_pgsql.dll and remove the semicolon at the beginning
    5. Find ;extension=php_pdo_pgsql.dll and remove the semicolon at the beginning
    6. Find ;extension="pgsql.so" and remove the semicolon at the beginning
    7. Save the file
    8. Restart Apache